Introduction
============

A dumb cURL impostor to illustrate Python development.

The ``suddenthought.crul`` Python distribution package demonstrates how to use
the tools below.

- the `Python <https://docs.python.org/3/>`_ programming language
- the |unittest|_ package for unit testing
- the `PyBuilder <https://pybuilder.io/>`_ build tool
- the `Sphinx <https://www.sphinx-doc.org/>`_ documentation tool

.. |unittest| replace:: ``unittest``
.. _unittest: https://docs.python.org/3/library/unittest.html
