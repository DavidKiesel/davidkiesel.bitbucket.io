Development
===========

Create the Repository Directory
-------------------------------

Starting in the directory in which you normally store projects (e.g.,
``${HOME}/src/python``), execute the commands below, replacing the value of
``python-crul`` as needed.

.. code-block:: bash

   mkdir python-crul

   cd "$_"

   git init

   git checkout -b trunk

Note that, in git versions 2.28+, the two git commands can be replaced with a
single git command ``git init --initial-branch trunk``.

Create file ``.gitignore`` to prevent git from including extraneous files.  See
`github/gitignore <https://github.com/github/gitignore>`_.

Choosing a License
------------------

For discussions on licensing, see `Choose an open source license
<https://choosealicense.com/>`_ and `GNU licenses
<https://www.gnu.org/licenses/>`_.

For a long list of licenses with identifiers and license text, see `SPDX
License List <https://spdx.org/licenses/>`_.

Some thoughts...

If you are ever going to regret someone using your code and making a gazillion
dollars of which you will not see one dime, do not share the code.

If you are fine with someone using your code and making a gazillion dollars of
which you will not see one dime but you want to prevent them from modifying and
distributing the software without "sharing back", consider a GPL or LGPL
license.

If you are fine with someone using your code and making a gazillion dollars of
which you will not see one dime and all you want is for your license to carry
forward with respect to your code ("See, I wrote this!"), consider the MIT
license.

Another things to consider is how you would prefer to handle code contributions
from others and the legal requirements...

Applying a license...

Add the full license text to a ``LICENSE`` file in the repository directory.
This can be done right away.

GNU further recommends applying a copyright and license header to each file.
E.g.,

.. code-block:: python

   ##############################################################################
   # copyrights and license
   #
   # Copyright (c) 2020 David Harris Kiesel
   #
   # Licensed under the MIT License. See LICENSE in the project root for license
   # information.
   ##############################################################################

Set the value of the ``build.py`` project attribute ``license`` to the SPDX
license identifier.  You will have to wait to do this until after a later step
in this document.  E.g.,

.. code-block:: python

   license = 'MIT'

Add a license element to the ``build.py`` project property
``distutils_classifiers``.  See `PyPI classifiers
<https://pypi.org/classifiers/>`_.  You will have to wait to do this until
after a later step in this document.  E.g.,

.. code-block:: python

   @init
   def set_properties(
       project
   ):
       ...

       project.set_property(
           'distutils_classifiers',
           [
               'Development Status :: 3 - Alpha',
               'Environment :: Console',
               'Intended Audience :: Developers',
               'License :: OSI Approved :: MIT License',
               'Programming Language :: Python',
               'Programming Language :: Python :: 3 :: Only',
               'Programming Language :: Python :: 3.9'
           ]
       )

Set Python Versions
-------------------

Repository file ``.python-version`` signals to the ``pyenv`` ``python`` shim,
when executed in the repository directory or below, to use the Python version
in the file.

The sections below describe using separate Python environments for running code
and for building code.  It is probably advisable that the Python executables be
at the same version.

``.python-version-build`` will be set to a "standard" ``pyenv`` virtual
environment.  When it is time to use a "build" command (e.g., ``pyb``,
``flake8``, ``twine``), the ``PYENV_VERSION`` environment variable can be set
to contain the contents of file ``.python-version-build``.  Or
``.python-version`` can be temporarily set to the contents of
``.python-version-build``.

``.python-version-run`` and ``.python-version`` (normally) will be set to a
given ``pyenv`` Python version.  The virtual environment for running code will
be created with ``virtualenv`` and stored in a ``venv`` directory under the
repository directory.

Set Python and Virtual Environment for Building Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The approach given here makes use of a consistent set of build tools for each
version of Python used for development.

For the prerequisite instructions on installing a ``pyenv`` virtual environment
for build tools, see section :ref:`PyBuilder Install`.

To see the list of currently installed ``pyenv`` virtual environments
available, execute the command below.

.. code-block:: bash

   pyenv virtualenvs

To set file ``.python-version``, from the repository directory, execute a
command like the one below.  Note that the Python executable version should be
the same as the one intended for running code.

.. code-block:: bash

   pyenv \
       local \
       3.8.6-pyb-0.12.10

To capture the current set of packages installed in the ``pyenv`` virtual
environment, execute the command below.

.. code-block:: bash

   pip \
       freeze \
       >requirements-python-version-build.txt

To move ``.python-version`` to a build-specific file, from the repository
directory, execute the command below.  The intent is that ``.python-version``
will normally be set as needed for running code, not building code.  When it is
time to use ``pyb`` for building code, the ``PYENV_VERSION`` environment
variable can be set to contain the contents of file
``.python-version-build``.

.. code-block:: bash

   mv \
      .python-version \
      .python-version-build

Set Python for Running Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To see the list of currently installed ``pyenv`` Python versions available,
execute the command below.

.. code-block:: bash

   pyenv versions

To set file ``.python-version``, from the repository directory, execute a
command like the one below.  Note that the Python executable version should be
the same as the one intended for building code.

.. code-block:: bash

   pyenv \
       local \
       3.8.6

To copy ``.python-version`` to a run-specific file, from the repository
directory, execute the command below.  The intent is that ``.python-version``
will normally be set as needed for running code, not building code.

.. code-block:: bash

   cp \
      -a \
      .python-version \
      .python-version-run

PyBuilder Initialization
------------------------

Create Initial PyBuilder Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create initial PyBuilder files, from the repository directory, execute the
command below.  When prompted, each of the defaults should be satisfactory. 

.. code-block:: bash

   PYENV_VERSION=$(cat .python-version-build) \
   pyb \
       --start-project

See below for an example session.

.. code-block:: text 

   Project name (default: 'python-crul') : 
   Source directory (default: 'src/main/python') : 
   Docs directory (default: 'docs') : 
   Unittest directory (default: 'src/unittest/python') : 
   Scripts directory (default: 'src/main/scripts') : 
   Use plugin python.flake8 (Y/n)? (default: 'y') : 
   Use plugin python.coverage (Y/n)? (default: 'y') : 
   Use plugin python.distutils (Y/n)? (default: 'y') : 

   Created 'setup.py'.

   Created 'pyproject.toml'.

``pyb`` will have created the files below.

``build.py``
    This is the PyBuilder configuration file.

``pyproject.toml``
    Specifies build system dependencies per `PEP 518
    <https://www.python.org/dev/peps/pep-0518/>`_.

``setup.py``
    Supports installation via ``pip install git+git://<project>@<branch>`` in
    conjunction with PyBuilder.

Additionally, ``pyb`` will have created the directories below.

``src/main/python``
    Normal Python code is stored under here.
``src/main/scripts``
    Python scripts are stored under here.
``src/unittest/python``
    Python unit test code is stored under here.

Customize ``build.py``
^^^^^^^^^^^^^^^^^^^^^^

Customize ``build.py`` as needed.

* add ``import`` statements needed within ``build.py``

   * e.g.,

   .. code-block:: python

      from pybuilder.core import \
          Author, \
          init, \
          task, \
          use_bldsup, \
          use_plugin

* use `use_bldsup
  <https://pybuilder.io/documentation/plugins#additional-project-structure>`_
  to specify a directory (e.g., directory ``bldsup``) containing additional
  Python modules (e.g., file ``build_util.py``) to import for use within
  ``build.py``

   * this can be used to prevent clutter in file ``build.py``
   * e.g.,

   .. code-block:: python

      # bldsup
      # https://pybuilder.io/documentation/plugins#additional-project-structure
      use_bldsup(
          build_support_dir='bldsup'
      )
      import build_util

* add PyBuilder `plugins <https://pybuilder.io/documentation/plugins>`_

   * e.g.,

   .. code-block:: python

      use_plugin('python.core')
      use_plugin('python.install_dependencies')
      use_plugin('python.flake8')
      use_plugin('python.unittest')
      #use_plugin('python.integrationtest')
      use_plugin('python.coverage')
      use_plugin('python.distutils')
      # https://www.sphinx-doc.org/en/master/
      # Commented out because couldn't get this to work quite right.
      #use_plugin('python.sphinx')

* add project attributes as globally scoped variables

   * e.g.,

   .. code-block:: python

      name = 'suddenthought.crul'

* add project properties through function calls within an initializer

   * e.g.,

   .. code-block:: python

      @init
      def set_properties(
          project
      ):
          ##########################################################################
          # install_dependencies plugin properties
          #
          # https://pybuilder.io/documentation/plugins

          project.depends_on_requirements(
              'requirements.txt'
          )

          project.depends_on_requirements(
              'requirements-dev.txt'
          )

* add `custom tasks <https://pybuilder.io/documentation/writing-plugins>`_ as
  globally scoped functions with the ``@task`` decorator and parameters
  ``project`` and ``logger``

   * e.g.,

   .. code-block:: python

      # https://pybuilder.io/documentation/writing-plugins
      @task(
          description='Example of a custom task.'
      )
      def some_task(
          project,
          logger
      ):
          print('Hello, build.')

Project Attributes
^^^^^^^^^^^^^^^^^^

``name`` (``str``)
    Applied to distribution ``setup.py`` variable ``name``.

``version`` (``str``)
    Applied to distribution ``setup.py`` variable ``version``.

``default_task`` (``str`` or ``list`` of ``str``)
    Default task or tasks for ``pyb`` to execute.

``summary`` (``str``)
    Applied to distribution ``setup.py`` variable ``description``.

``description`` (``str``)
    Applied to distribution ``setup.py`` variable ``long_description``.

``authors`` (``list`` of ``pybuilder.core.Author``)
    Applied to distribution ``setup.py`` variables ``author`` and
    ``author_email``.

``maintainers`` (``list`` ``pybuilder.core.Author``)
    Applied to distribution ``setup.py`` variables ``maintainer`` and
    ``maintainer_email``.

``license`` (``str``)
    Applied to distribution ``setup.py`` variable ``license``.

``url`` (``str``)
    Applied to distribution ``setup.py`` variable ``url``.

``urls`` (``dict``)
    Applied to distribution ``setup.py`` variable ``project_urls``.

``explicit_namespaces`` (``list`` of ``str``)
    Applied to distribution ``setup.py`` variable ``namespace_packages``.

``requires_python`` (``str``)
    Applied to distribution ``setup.py`` variable ``python_requires``.

``obsoletes`` (``list`` of ``str``)
    Applied to distribution ``setup.py`` variable ``obsoletes``.

References:

* `PyBuilder - Plugins <https://pybuilder.io/documentation/plugins>`_
* `core.py <https://github.com/pybuilder/pybuilder/blob/master/src/main/python/pybuilder/core.py>`_
* `reactor.py <https://github.com/pybuilder/pybuilder/blob/master/src/main/python/pybuilder/reactor.py>`_
* `Packaging namespace packages <https://packaging.python.org/guides/packaging-namespace-packages/>`_

Project Properties
^^^^^^^^^^^^^^^^^^

References:

* `PyBuilder - Plugins <https://pybuilder.io/documentation/plugins>`_

Install Packages
----------------

To install packages required by the repository code, from the repository
directory, execute commands like below, changing the arguments to ``pip
install`` as needed.

.. code-block:: bash

   virtualenv \
       --python $(pyenv which python3) \
       venv

   . venv/bin/activate

   pip \
       install \
       requests

   pip \
       freeze \
       >requirements.txt

   deactivate

Add Code
--------

As needed...

Lint Code
---------

To `lint <https://en.wikipedia.org/wiki/Lint_(software)>`_ the code, from the
repository directory, execute the command below.

.. code-block:: bash

   PYENV_VERSION=$(cat .python-version-build) \
   flake8 \
       src

Commit Code
-----------

As needed...

E.g.,

.. code-block:: bash

   git status

   git add --dry-run .

   git add .

   git commit

Execute PyBuilder Tasks
-----------------------

To list available PyBuilder tasks, from the repository directory, execute the
command below.

.. code-block:: bash

   PYENV_VERSION=$(cat .python-version-build) \
   pyb --list-tasks

To list default PyBuilder tasks and task dependencies, from the repository
directory, execute the command below.

.. code-block:: bash

   PYENV_VERSION=$(cat .python-version-build) \
   pyb --list-plan-tasks

To list PyBuilder tasks and task dependencies that will be executed for a given
task, from the repository directory, execute the command below, replacing
``TASK`` as needed.

.. code-block:: bash

   PYENV_VERSION=$(cat .python-version-build) \
   pyb --list-plan-tasks TASK

To execute default PyBuilder tasks and task dependencies, from the repository
directory, execute the command below.  See the `default_task` variable in file
`build.py`.

.. code-block:: bash

   PYENV_VERSION=$(cat .python-version-build) \
   pyb

Sphinx Initialization
---------------------

Sphinx is a software documentation generator.

Ideally, the ``python.sphinx`` plug-in for PyBuilder could be used to control
Sphinx.  However, I had difficulty getting it to work.  So, the approach here
will be to create a virtual environment local to repository and use Sphinx
through it.

References:

- `Sphinx at pypi.org <https://pypi.org/project/Sphinx/>`_
- `Sphinx Homepage <https://www.sphinx-doc.org/en/master/>`_

Install Sphinx in a Virtual Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install Sphinx in a virtual environment local to repository, from the
repository directory, execute the commands below.

.. code-block:: bash

   cd docs

   pyenv \
       local \
       "$(python --version | cut -d ' ' -f 2)"

   virtualenv \
       --python $(pyenv which python3) \
       venv

   . venv/bin/activate

   pip \
       install \
       sphinx

   pip \
       freeze \
       >../requirements-sphinx.txt

   deactivate

   rm -rf venv

   virtualenv \
       --python $(pyenv which python3) \
       venv

   . venv/bin/activate

   pip \
       install \
       --requirement ../requirements.txt \
       --requirement ../requirements-sphinx.txt

   deactivate


Create Initial Sphinx Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create initial Sphinx files, from the repository directory, execute the
commands below.  When prompted with ``Separate source and build directories``,
respond with `y`.  When prompted with ``Project name``, respond as appropriate
(e.g., ``python-crul``).  When prompted with ``Author name(s)``, respond as
appropriate.  When prompted with ``Project release``, respond as appropriate
(e.g., ``1.0.0.dev``).  When prompted with ``Project language``, respond with
``en``.

.. code-block:: bash

   (
       cd docs

       . venv/bin/activate

       sphinx-quickstart

       deactivate
   )

See below for an example session.

.. code-block:: text 

   Welcome to the Sphinx 3.3.1 quickstart utility.

   Please enter values for the following settings (just press Enter to
   accept a default value, if one is given in brackets).

   Selected root path: .

   You have two options for placing the build directory for Sphinx output.
   Either, you use a directory "_build" within the root path, or you separate
   "source" and "build" directories within the root path.
   > Separate source and build directories (y/n) [n]: y

   The project name will occur in several places in the built documentation.
   > Project name: python-crul
   > Author name(s): David Harris Kiesel
   > Project release []: 1.0.0.dev

   If the documents are to be written in a language other than English,
   you can select a language here by its language code. Sphinx will then
   translate text that it generates into that language.

   For a list of supported codes, see
   https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
   > Project language [en]: 

   Creating file /home/user/Documents/src/python/python-crul/docs/source/conf.py.
   Creating file /home/user/Documents/src/python/python-crul/docs/source/index.rst.
   Creating file /home/user/Documents/src/python/python-crul/docs/Makefile.
   Creating file /home/user/Documents/src/python/python-crul/docs/make.bat.

   Finished: An initial directory structure has been created.

   You should now populate your master file /home/user/Documents/src/python/python-crul/docs/source/index.rst and create other documentation
   source files. Use the Makefile to build the docs, like so:
      make builder
   where "builder" is one of the supported builders, e.g. html, latex or linkcheck.

``sphinx-quickstart`` creates the files and directories below under directory
``docs``.

* ``build``

    * directory under which Sphinx output is written

* ``source``

    * directory under which Sphinx source files are stored
    * ``conf.py``

        * Sphinx `configuration file
          <https://www.sphinx-doc.org/en/master/usage/configuration.html>`_

    * ``index.rst``

        * the `master document
          <https://www.sphinx-doc.org/en/master/glossary.html#term-master-document>`_,
          the initial documentation page, formatted as `reStructuredText
          <https://www.sphinx-doc.org/en/master/usage/restructuredtext/>`_)

    * ``_static``

        * directory under which static source files (e.g., style sheets or
          script files) are stored; contents are copied under the output's
          ``_static`` directory after the theme files

    * ``_templates``

        * directory under which HTML `template files
          <https://www.sphinx-doc.org/en/master/templating.html>`_ are stored

* ``make.bat``

    * a batch file for building documentation from Sphinx files

* ``Makefile``

    * a ``makefile`` for building documentation from Sphinx files

References:

* `Sphinx <https://www.sphinx-doc.org/en/master/>`_
* `Sphinx - reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/>`_
* `Introduction to reStructuredText <https://www.writethedocs.org/guide/writing/reStructuredText/>`_

Create Sphinx API Files
^^^^^^^^^^^^^^^^^^^^^^^

To create Sphinx API files with ``automodule`` directives based on current
source code, from the repository directory, execute commands like the ones
below, modifying the final argument of the ``sphinx-apidoc`` command as
needed.

.. code-block:: bash

   (
       cd docs

       . venv/bin/activate

       sphinx-apidoc \
           --force \
           --separate \
           --implicit-namespaces \
           --module-first \
           --output-dir source/api \
           ../src/main/python/suddenthought

       deactivate
   )

Add file ``docs/source/api.rst``.  E.g.,

.. code-block:: restructuredtext

   API
   ===

   .. toctree::
      :maxdepth: 2
      
      api/modules

Create Sphinx Unit Text API Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create Sphinx unit test API files with ``automodule`` directives based on
current source code, from the repository directory, execute commands like the
ones below, modifying the final argument of the ``sphinx-apidoc`` command as
needed.

.. code-block:: bash

   (
       cd docs

       . venv/bin/activate

       sphinx-apidoc \
           --force \
           --separate \
           --implicit-namespaces \
           --module-first \
           --output-dir source/unit_test_api \
           ../src/unittest/python/suddenthought

       deactivate
   )

Add file ``docs/source/unit_test_api.rst``.  E.g.,

.. code-block:: restructuredtext

   Unit Test API
   =============

   .. toctree::
      :maxdepth: 2
      
      unit_test_api/modules

Customize Sphinx File ``conf.py``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Modify ``Path setup`` section.  E.g.,

.. code-block:: python

   import os
   import sys

   sys.path.insert(
       0,
       os.path.abspath(
           os.path.join(
               '..',
               '..',
               'src',
               'main',
               'python'
           )
       )
   )

   sys.path.insert(
       0,
       os.path.abspath(
           os.path.join(
               '..',
               '..',
               'src',
               'unittest',
               'python'
           )
       )
   )

Add elements to ``extensions`` list as needed.  E.g.,

.. code-block:: python

   extensions = [
       'sphinx.ext.autodoc',           # Grabs documentation from inside modules
       'sphinx.ext.autosectionlabel',  # Allow reference sections using its title
       'sphinx.ext.intersphinx'        # Link to other projects' documentation
   ]

Add ``autodoc`` options.  E.g.,

.. code-block:: python

   # -- Options for autodoc -----------------------------------------------------

   # https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#confval-autodoc_default_options
   autodoc_default_options = \
       {
           'special-members': '__init__'
       }

Add ``intersphinx`` options.  E.g.,

.. code-block:: python

   # -- Options for intersphinx -------------------------------------------------

   # https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#configuration
   intersphinx_mapping = \
       {
           'python': (
               'https://docs.python.org/3',
               None
           )
       }

Add Sphinx Source Files
^^^^^^^^^^^^^^^^^^^^^^^

As needed...

Modify Sphinx Root `index.rst`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As needed...

E.g.,

.. code-block:: restructuredtext

   Welcome to python-crul's documentation!
   =======================================

   Contents
   ========

   .. toctree::
      :maxdepth: 2

      user_guide/index
      developer_guide/index
      api
      unit_test_api

   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

Build Documentation Using Sphinx
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To build the documentation using Sphinx, from the repository directory, execute
the commands below.

.. code-block:: bash

   (
       cd docs

       . venv/bin/activate

       sphinx-build \
           source \
           build

       deactivate
   )

Install a ``target`` Wheel File Using ``pipx``
----------------------------------------------

To install the current ``target`` wheel file using ``pipx``, from the
repository directory, execute the command below.

.. code-block:: bash

   pipx \
       install \
       --python $(pyenv which python3) \
       target/dist/*/dist/*.whl

To list packages installed using ``pipx``, execute the command below.

.. code-block:: bash

   pipx list

Any scripts under ``src/main/scripts`` should now be executable.  E.g.,

.. code-block:: bash

   which crul

   crul --help

   crul 'https://example.com'

To uninstall the package, execute the command below, replacing ``PACKAGE`` as
needed.

.. code-block:: bash

   pipx uninstall PACKAGE
