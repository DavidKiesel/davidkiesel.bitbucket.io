<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>4.2.3.&nbsp;Interpreting Query Job Information</title><link rel="stylesheet" type="text/css" href="css/article.css"><meta name="generator" content="DocBook XSL Stylesheets V1.79.1"><link rel="home" href="Title_Page.html" title="Google BigQuery Guide"><link rel="up" href="Troubleshooting_Queries.html" title="4.2.&nbsp;Troubleshooting Queries"><link rel="prev" href="Query_Job_Information_Via_Command_Line.html" title="4.2.2.&nbsp;Query Job Information Via Command Line"><link rel="next" href="Troubleshooting_Queries_-_Checklist.html" title="4.2.4.&nbsp;Troubleshooting Queries - Checklist"><base target="right_frame"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">4.2.3.&nbsp;Interpreting Query Job Information</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="Query_Job_Information_Via_Command_Line.html">Prev</a>&nbsp;</td><th width="60%" align="center">4.2.&nbsp;Troubleshooting Queries</th><td width="20%" align="right">&nbsp;<a accesskey="n" href="Troubleshooting_Queries_-_Checklist.html">Next</a></td></tr></table><hr></div><div class="section"><div class="titlepage"><div><div><h4 class="title"><a name="Interpreting_Query_Job_Information"></a>4.2.3.&nbsp;Interpreting Query Job Information</h4></div></div></div><p>
        This section will provide some information on how to interpret query
        job information.
    </p><p>
        Google maintains documentation about BigQuery job information at the
        links below.

        </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
                    <a class="link" href="https://cloud.google.com/bigquery/docs/reference/rest/v2/Job" target="_top">https://cloud.google.com/bigquery/docs/reference/rest/v2/Job</a>
                </p></li><li class="listitem"><p>
                    <a class="link" href="https://cloud.google.com/bigquery/query-plan-explanation" target="_top">https://cloud.google.com/bigquery/query-plan-explanation</a>
                </p></li></ul></div><p>
    </p><p>
        The figure below depicts how contiguous and overlapping job stage
        intervals can be grouped into islands, where there is constantly one or
        more slots allocated to the job stage.  And the intervals between
        islands form gaps, where there are no slots allocated.  Stages 0 and 1
        are contiguous, and stage 2 overlaps with stage 1; stages 0, 1, and 2
        form an island.  Stage 3 executes alone and forms its own island.
        Between stages island 1 and stages island 2 there is a gap.  The
        motivation for determining the islands and gaps is to determine when
        one or more slots are allocated (good) and when no slots are allocated
        (bad).  Note that terms used in this figure are described below.
    </p><pre class="screen">
    -- stage 0 startTime ----------------------------
    |                                              |
    |  stage 0 interval                            |  stages island 1
    |                                              |
    -- stage 0 endTime -------- stage 1 startTime  |
                             |                     |
    -- stage 2 startTime     |  stage 1 interval   |
    |                        |                     |
    |  stage 2 interval      -- stage 1 endTime    |
    |                                              |
    -- stage 2 endTime ------------------------------
    |
    |  stages gap 1
    |
    -- stage 3 startTime  --
    |                     |
    |  stage 3 interval   |  stages island 2
    |                     |
    -- stage 3 endTime------
    |
    | time
    V
    </pre><p>
        The figure below depicts times and intervals in the life cycle of a
        query job.  Note that terms used in this figure are described below.
    </p><pre class="screen">
    -- job creationTime ----------------------------------
    |                                                   |
    |  PENDING                                          |  in-flight
    |                                                   |
    -- job startTime -------------------------          |
    |                                       |           |
    |  unstaged                             |  RUNNING  |
    |                                       |           |
    -- stages_start_time ----------         |           |
    |                            |          |           |
    |  staged island (active)    |  staged  |           |
    |                            |          |           |
    -- staged island end time    |          |           |
    |                            |          |           |
    |  staged gap (inactive)     |          |           |
    |                            |          |           |
    -- staged island start time  |          |           |
    |                            |          |           |
    |  staged island (active)    |          |           |
    |                            |          |           |
    -- stages_end_time ------------         |           |
    |                                       |           |
    |  ending                               |           |
    |                                       |           |
    -- job endTime ---------------------------------------
    |
    |  DONE
    |
    |
    | time
    V
    </pre><p>
        The terms used in the figures above are described below.  Some of the
        terms are defined in Google documentation, and some are defined only in
        this guide.  Google terms which are field names tend to be written in
        lower camel case in the REST API (e.g., field <span class="database">creationTime</span>) and in snake case in
        <span class="database">INFORMATION_SCHEMA</span> tables (e.g., field <span class="database">creation_time</span>).  BigQuery tends to natively
        record times as <span class="database">integer</span>
        milliseconds since the <a class="link" href="https://en.wikipedia.org/wiki/Unix_time" target="_top">UNIX
        epoch</a>, but it may be converted to a <span class="database">timestamp</span>.
    </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">
                <p>
                    <span class="database">creationTime</span>
                </p>
            </span></dt><dd><p>
                    Or <span class="database">creation_time</span>.
                    Google field that indicates when BigQuery first registers a
                    job due to a job insert request.  Note that a job insert
                    request that is rejected because it would violate the
                    project's job concurrency limit is not recorded as part of
                    the standard jobs information.
                </p></dd><dt><span class="term">
                <p>
                    <code class="literal">in-flight</code>
                </p>
            </span></dt><dd><p>
                    Interval between <span class="database">creationTime</span> and job <span class="database">endTime</span>.  Google field <span class="database">state</span> has value
                    <code class="literal">PENDING</code> or <code class="literal">RUNNING</code>
                    during this interval, not <code class="literal">DONE</code>.
                </p><p>
                    Note that some query job information which should logically
                    be available while a query job is in-flight may not
                    actually available while a query job is in-flight.
                    That information is only available once a query job is
                    <code class="literal">DONE</code>.
                </p><p>
                    Below are some job stage attributes and metrics that may be
                    misleading while a query job is in-flight.
                </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
                            <span class="database">name</span>
                        </p><p>
                            When a stage <span class="database">status</span> is
                            <code class="literal">COMPLETE</code>, <span class="database">name</span> will indicate
                            something about the operations performed in the
                            stage (e.g., <code class="literal">S00: Input</code> for a
                            first stage, which reads from a table).  However,
                            while a stage <span class="database">status</span> is
                            <code class="literal">RUNNING</code>, <span class="database">name</span> will temporarily
                            indicate an <code class="literal">Output</code> operation
                            (e.g., <code class="literal">S00: Output</code>).
                        </p></li><li class="listitem"><p>
                            <span class="database">startMs</span>
                        </p><p>
                            This field (and any field derived from it) is
                            <code class="literal">null</code> while the query job is
                            in-flight.
                        </p></li><li class="listitem"><p>
                            <span class="database">endMs</span>
                        </p><p>
                            This field (and any field derived from it) is
                            <code class="literal">null</code> while the query job is
                            in-flight.
                        </p></li><li class="listitem"><p>
                            <span class="database">computeMsAvg</span>,
                            <span class="database">computeMsMax</span>
                        </p><p>
                            These fields may be overstated while the query job
                            is in-flight.
                        </p></li><li class="listitem"><p>
                            <span class="database">waitMsAvg</span>,
                            <span class="database">waitMsMax</span>
                        </p><p>
                            These fields will be <code class="literal">0</code> while the
                            query job is in-flight, but set to their true value
                            when the query job is <code class="literal">DONE</code>.
                        </p></li></ul></div></dd><dt><span class="term">
                <p>
                    <code class="literal">PENDING</code>
                </p>
            </span></dt><dd><p>
                    Google term for interval between <span class="database">creationTime</span> and job <span class="database">startTime</span>.  Google field <span class="database">state</span> has value
                    <code class="literal">PENDING</code> during this interval.
                </p></dd><dt><span class="term">
                <p>
                    job <span class="database">startTime</span>
                </p>
            </span></dt><dd><p>
                    Or <span class="database">start_time</span>.  Google
                    field that indicates when BigQuery first attempts to start
                    work to generate query results.
                </p></dd><dt><span class="term">
                <p>
                    <code class="literal">RUNNING</code>
                </p>
            </span></dt><dd><p>
                    Google term for interval between <span class="database">startTime</span> and job <span class="database">endTime</span>.  Google field <span class="database">state</span> has value
                    <code class="literal">RUNNING</code> during this interval.
                </p></dd><dt><span class="term">
                <p>
                    unstaged
                </p>
            </span></dt><dd><p>
                    Interval between <span class="database">startTime</span> and <span class="database">stages_start_time</span>.  No slots are
                    assigned to do work for the query job during this interval.
                </p></dd><dt><span class="term">
                <p>
                    <span class="database">stages_start_time</span>
                </p>
            </span></dt><dd><p>
                    Computed field that indicates the earliest start time of
                    any stage.  E.g., <code class="code">min(start_ms) from
                    unnest(job_stages)</code>.
                </p></dd><dt><span class="term">
                <p>
                    staged island (active)
                </p>
            </span></dt><dd><p>
                    One or more intervals during which one or more contiguous
                    or overlapping stages executed.  It is only during these
                    intervals that slots are assigned to do work.
                </p><p>
                    The start of an interval is one of:

                    </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
                                <span class="database">stages_start_time</span>
                            </p></li><li class="listitem"><p>
                                stages gap start time
                            </p></li></ul></div><p>
                </p><p>
                    The end of an interval is one of:

                    </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
                                stages gap end time
                            </p></li><li class="listitem"><p>
                                <span class="database">stages_end_time</span>
                            </p></li></ul></div><p>
                </p></dd><dt><span class="term">
                <p>
                    stages gap start time
                </p>
            </span></dt><dd><p>
                    Zero, one, or more computed times indicating the start of a
                    stages gap, or equivalently, the end of a first or
                    intermediate stages island.  Computed as the maximum stage
                    <span class="database">endMs</span> of a stages
                    island, which is composed of one or more contiguous or
                    overlapping stages.
                </p></dd><dt><span class="term">
                <p>
                    staged gap (inactive)
                </p>
            </span></dt><dd><p>
                    Zero, one, or more intervals during which there is a gap
                    between stage islands.  So, no stages occur during this
                    interval, but there is at least one prior interval with one
                    or more stages.  And more stages are required to complete
                    the job.
                </p><p>
                    The start of an interval is a staged island end time.
                </p><p>
                    The end of an interval is a staged island start time
                </p></dd><dt><span class="term">
                <p>
                    stages gap end time
                </p>
            </span></dt><dd><p>
                    Zero, one, or more computed times indicating the end of a
                    stages gap, or equivalently, the start of an intermediate
                    or final stages island.  Computed as the minimum stage
                    <span class="database">startMs</span> of a stages
                    island, which is composed of one or more contiguous or
                    overlapping stages.
                </p></dd><dt><span class="term">
                <p>
                    <span class="database">stages_end_time</span>
                </p>
            </span></dt><dd><p>
                    Computed field that indicates the last end time of any
                    stage: <code class="code">max(end_ms) from unnest(job_stages)</code>.
                </p></dd><dt><span class="term">
                <p>
                    ending
                </p>
            </span></dt><dd><p>
                    Interval between <span class="database">stages_end_time</span> and job <span class="database">endTime</span>.
                </p></dd><dt><span class="term">
                <p>
                    job <span class="database">endTime</span>
                </p>
            </span></dt><dd><p>
                    Or <span class="database">end_time</span>.  Google
                    field that indicates the end of the job.  At this point,
                    the destination table, if any, should be populated and its
                    data available for a table list operation.
                </p></dd><dt><span class="term">
                <p>
                    <code class="literal">DONE</code>
                </p>
            </span></dt><dd><p>
                    Google term for final state when the query job has
                    completed, after <span class="database">endTime</span>.  Google field <span class="database">state</span> has value
                    <code class="literal">DONE</code> now and hereafter.
                </p></dd></dl></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="Query_Job_Information_Via_Command_Line.html">Prev</a>&nbsp;</td><td width="20%" align="center"><a accesskey="u" href="Troubleshooting_Queries.html">Up</a></td><td width="40%" align="right">&nbsp;<a accesskey="n" href="Troubleshooting_Queries_-_Checklist.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">4.2.2.&nbsp;Query Job Information Via Command Line&nbsp;</td><td width="20%" align="center"><a accesskey="h" href="Title_Page.html">Home</a>&nbsp;|&nbsp;<a accesskey="t" href="ar01-toc.html">ToC</a></td><td width="40%" align="right" valign="top">&nbsp;4.2.4.&nbsp;Troubleshooting Queries - Checklist</td></tr></table></div></body></html>